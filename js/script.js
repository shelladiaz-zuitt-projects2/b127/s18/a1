
/***

let movies = {
    description: [
        {
            title: "avengers", 
            genre: "action", 
            releaseDate: new Date(2021, 01, 01),
            rating: 5
        },
        {
            title: "batman", 
            genre: "fiction", 
            releaseDate: new Date(2021, 01, 02),
            rating: 5
        },
        {
            title: "wonderwoman", 
            genre: "comedy", 
            releaseDate: new Date(2021, 01, 03),
            rating: 5
        },
        {
            title: "handel and gretel", 
            genre: "fantasy", 
            releaseDate: new Date(2021, 01, 04),
            rating: 5
        },
        {
            title: "one punch man", 
            genre: "action", 
            releaseDate: new Date(2021, 01, 05),
            rating: 5
        }
    ],
    displayRating: function(){
        console.log("the movie " + this.description[0].title + " has " + this.description[0].rating + " stars.");
        console.log("the movie " + this.description[1].title + " has " + this.description[1].rating + " stars.");
        console.log("the movie " + this.description[2].title + " has " + this.description[2].rating + " stars.");
        console.log("the movie " + this.description[3].title + " has " + this.description[3].rating + " stars.");
        console.log("the movie " + this.description[4].title + " has " + this.description[4].rating + " stars.");
    },
    // showMovies: function(){
    //     console.log(this.description[0].title + " is an " + this.description[0].genre);
    //     console.log(this.description[1].title + " is an " + this.description[1].genre);
    //     console.log(this.description[2].title + " is an " + this.description[2].genre);
    //     console.log(this.description[3].title + " is an " + this.description[3].genre);
    //     console.log(this.description[4].title + " is an " + this.description[4].genre);
    // }
 
} 

// movies.displayRating();
// movies.showAllMovies();
*/
    
let movies =[
        {
            title: "avengers", 
            genre: "action", 
            releaseDate: new Date(2021, 01, 01),
            rating: 5,

            displayRating: function(){
                return("the movie " + this.title + " has " + this.rating + " stars.");
            },
            showTitleGenre: function(){
                return(this.title + " is " + this.genre + " movie.");
            }
        },
        {
            title: "batman", 
            genre: "fiction", 
            releaseDate: new Date(2021, 01, 02),
            rating: 4,

            displayRating: function(){
                return("the movie " + this.title + " has " + this.rating + " stars.");
            },
            showTitleGenre: function(){
                return(this.title + " is " + this.genre + " movie.");
            }
        },
        {
            title: "wonderwoman", 
            genre: "comedy", 
            releaseDate: new Date(2021, 01, 03),
            rating: 3,

            displayRating: function(){
                return("the movie " + this.title + " has " + this.rating + " stars.");
            },
            showTitleGenre: function(){
                return(this.title + " is " + this.genre + " movie.");
            }
        },
        {
            title: "handel and gretel", 
            genre: "fantasy", 
            releaseDate: new Date(2021, 01, 04),
            rating: 2,

            displayRating: function(){
                return("the movie " + this.title + " has " + this.rating + " stars.");
            },
            showTitleGenre: function(){
                return(this.title + " is " + this.genre + " movie.");
            }
        },
        {
            title: "one punch man", 
            genre: "action", 
            releaseDate: new Date(2021, 01, 05),
            rating: 5,

            displayRating: function(){
                return("the movie " + this.title + " has " + this.rating + " stars.");
            },
            showTitleGenre: function(){
                return(this.title + " is " + this.genre + " movie.");
            }
        }
    ]



console.log(movies[1].displayRating());

function showAllMovies(arr){
    let m = 0;
    for(m = 0; m < movies.length; m++){
        console.log(movies[m].showTitleGenre());
    }
}

showAllMovies("any");





